/*
 *   C++ sockets on Unix and Windows
 *   Copyright (C) 2002
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <iostream>           // For cout, cerr
#include <cstdlib>            // For atoi()
#include <pthread.h>          // For POSIX threads
#include <signal.h>
#include "api/Server.h"
#include "model/DbContext.h"
#include "model/account.h"

using namespace cnp;
using namespace net;
using namespace model;

void HandleTCPClient(CnpSocket *sock);     // TCP client handling function
void *ThreadHandler(void *arg);               // Main program of a thread

int main(int argc, char *argv[]) {
    if (argc < 2) {                 // Test for correct number of arguments
        cerr << "Usage: " << argv[0] << " <Listen Port> [<sqlite db filename>] " << endl;
		exit(1);
	}

	signal(SIGHUP, SIG_IGN);
	signal(SIGPIPE, SIG_IGN);

	srand(time(0));
    unsigned short Port = atoi(argv[1]);    // First arg:  local port
    if (argc == 3) {
        DbContext::Init(argv[2]);
    } else {
        DbContext::Init();
    }

    cout << "Db file " << DbContext::getFilename() << endl;


	try {
        TCPServerSocket servSock(Port);  // Socket descriptor for server
        cout << "Listen on :" << Port;
		for (;;) {      // Run forever
			// Create separate memory for client argument
            CnpSocket *clntSock = servSock.accept();

			// Create client thread
			pthread_t threadID;              // Thread ID from pthread_create()
			if (pthread_create(&threadID, NULL, ThreadHandler,
					(void *) clntSock) != 0) {
				cerr << "Unable to create thread" << endl;
				exit(1);
			}
		}
	} catch (SocketException &e) {
		cerr << e.what() << endl;
		exit(1);
	}
	// NOT REACHED

	return 0;
}


void *ThreadHandler(void *clntSock) {
	// Guarantees that thread resources are deallocated upon return
	pthread_detach(pthread_self());

    // Send received string and receive again until the end of transmission
    CnpSocket *sock = (CnpSocket*)clntSock;
    Server handler(sock);
    handler.print();
    handler.loop();
	return NULL;
}
