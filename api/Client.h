#ifndef CLIENTHANDLER_H
#define CLIENTHANDLER_H

#include "net/CnpSocket.h"
#include "model/account.h"

using namespace cnp;
using namespace model;
using namespace net;

class Client {
private:
    WORD ClientId;
    CnpSocket* socket;

    Message *recv() {
         return socket->readMessage();
    }

    void send(Message *msg) {
        socket->writeMessage(msg);
    }

    Message *sendAndReceive(void *data);


    /*
    void deposit(DEPOSIT_REQUEST* msg);
    void withdrawl(WITHDRAWAL_REQUEST* msg);
    void queryBalance(BALANCE_QUERY_REQUEST* msg);
    void queryTransaction(TRANSACTION_QUERY_REQUEST* msg);
    void purchaseStamps(STAMP_PURCHASE_REQUEST* msg);
    */
public:
    WORD get_ClientID() {
        return ClientId;
    }

    CONNECT_RESPONSE* connect();
    CREATE_ACCOUNT_RESPONSE *createAccount(const char *FirstName, const char *LastName, const char *Email, WORD randomPIN);
    LOGON_RESPONSE* logon(const char *FirstName, WORD PIN);
    LOGOFF_RESPONSE* logoff();
    DEPOSIT_RESPONSE* deposit(DWORD Amount);
    WITHDRAWAL_RESPONSE *withdraw(DWORD Amount);
    BALANCE_QUERY_RESPONSE *queryBalance();
    TRANSACTION_QUERY_RESPONSE* queryTransaction(DWORD Offset, DWORD Limit);
    STAMP_PURCHASE_RESPONSE *purchaseStamps(DWORD Amount);

    Client(const string &foreignAddress, unsigned short foreignPort)
        {
        socket = new CnpSocket(foreignAddress, foreignPort);
    };

    virtual ~Client();
};

#endif // CLIENTHANDLER_H
