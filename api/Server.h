/*
 * ServerHandler.h
 *
 */

#ifndef SERVERHANDLER_H_
#define SERVERHANDLER_H_
#include "net/CnpSocket.h"
#include "model/account.h"


using namespace cnp;
using namespace net;


class Server {
private:
    model::Account *account;
    unsigned int seedp;
    CnpSocket* sock;
    pthread_t threadId;
    WORD clientID;
    int stop;
    int isSent;

    Message *recv() {
        return sock->readMessage();
    }

    void send(void *data, DWORD code = CER_SUCCESS, DWORD argc = 0, void* argv = NULL) {
        if (isSent) return;
        Message *m = ((Message*)data)->Response(code, argc, argv);
#ifdef DEBUG
        m->printAsResponse();
#endif
        sock->writeMessage(m);
        delete m;
        isSent = 1;
    };

    int handle(Message* msg);

    int connect(CONNECT_REQUEST* msg);
    int createAccout(CREATE_ACCOUNT_REQUEST* msg);
    int logon(LOGON_REQUEST* msg);
    int logoff(LOGOFF_REQUEST* msg);
    int deposit(DEPOSIT_REQUEST* msg);
    int withdrawl(WITHDRAWAL_REQUEST* msg);
    int queryBalance(BALANCE_QUERY_REQUEST* msg);
    int queryTransaction(TRANSACTION_QUERY_REQUEST* msg);
    int purchaseStamps(STAMP_PURCHASE_REQUEST* msg);

public:
    Server(CnpSocket* sock);
    void loop();
    void print();
	virtual ~Server();
};


#endif /* SERVERHANDLER_H_ */
