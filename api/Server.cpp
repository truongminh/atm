/*
 * ServerHandler.cpp
 *
 */

#include <iostream>
#include "Server.h"
#include "model/TransactionLog.h"

using namespace cnp;
using namespace net;
using namespace model;

Server::Server(CnpSocket* socket) : sock(socket), threadId(pthread_self()) {
    seedp = time(NULL) ^ threadId;
    stop = 0;
    clientID = 0;
    isSent = 0;
}

Server::~Server() {
    delete sock;
    std::cout << "Close client " << clientID << std::endl;
}

#ifdef DEBUG
#define _HANLE_DEBUG(msg) do { \
        std::cout<< "Handle " << msg << " " << pthread_self() << std::endl; \
    } while(0)
#else
#define _HANLE_DEBUG(msg) (void)msg;
#endif

int Server::handle(Message* msg) {
	DWORD msgType = msg->get_MsgType();
    int res = 0;
	switch (msgType) {
	case MT_CONNECT_REQUEST_ID:
        res = connect((CONNECT_REQUEST*) msg);
		break;
    case MT_CREATE_ACCOUNT_REQUEST_ID:
        res = createAccout((CREATE_ACCOUNT_REQUEST*) msg);
		break;
	case MT_LOGON_REQUEST_ID:
        res = logon((LOGON_REQUEST*) msg);
		break;

	case MT_LOGOFF_REQUEST_ID:
        res = logoff((LOGOFF_REQUEST*) msg);
		break;

    case MT_DEPOSIT_REQUEST_ID:
        res = deposit((DEPOSIT_REQUEST*) msg);
		break;

    case MT_WITHDRAWAL_REQUEST_ID:
        res = withdrawl((WITHDRAWAL_REQUEST*) msg);
		break;

	case MT_BALANCE_QUERY_REQUEST_ID:
        res = queryBalance((BALANCE_QUERY_REQUEST*) msg);
		break;

	case MT_TRANSACTION_QUERY_REQUEST_ID:
        res = queryTransaction((TRANSACTION_QUERY_REQUEST*) msg);
		break;
	case MT_PURCHASE_STAMPS_REQUEST_ID:
        res = purchaseStamps((STAMP_PURCHASE_REQUEST*) msg);
        break;

    default:
        break;
	}
    return res;
}

#define DEBUG

void Server::loop() {
    Message* msg = NULL;

    try {
        while (!stop && (msg = recv()) != NULL) { // Zero means
#ifdef DEBUG
            msg->printAsRequest();
#endif
            try {
                isSent = 0;
                msg->isValidRequest();
                msg->checkClient(clientID);
                if (msg->requireLoggedOn()) {
                    if (account == NULL) {
                        throw MessageException(CER_CLIENT_NOT_LOGGEDON);
                    }
                }
                handle(msg);
                send(msg);
            } catch(MessageException &e)  {
                CER_TYPE cer = e.getError();
                if (cer == CER_UNSUPPORTED_PROTOCOL) {
                    // TODO: new msg
                    delete msg;
                    msg = new Message;
                }
                send(msg, cer);
            } catch(SQLite::Exception &e) {
                cerr << "SQLite  Error : " << e.what() << " " << pthread_self() << endl;
                send(msg, CER_ERROR);
            }

            delete msg;
            msg = NULL;
        }
    } catch (SocketException &e) {
        // cerr << "Send/Recv Error : " << e.what() << endl;
    } catch (exception &e) {
        cerr << "Handle " << pthread_self() << " Error: " << e.what() << endl;
    }

    if (msg != NULL) {
        msg->printAsRequest();
        delete msg;
    }
}

void Server::print() {
    try {
        cout << sock->getForeignAddress() << ":";
    } catch (SocketException &e) {
        cerr << "Unable to get foreign address" << endl;
    }
    try {
        cout << sock->getForeignPort();
    } catch (SocketException &e) {
        cerr << "Unable to get foreign port" << endl;
    }
    cout << " with thread " << threadId << endl;

}

/***********************************************/
int Server::connect(CONNECT_REQUEST* msg) {
    clientID = rand_r(&seedp);
    send(msg, CER_SUCCESS, clientID);
    return 1;
}

int  Server::createAccout(CREATE_ACCOUNT_REQUEST* msg) {
    _HANLE_DEBUG("Create Account");
    account = new Account(msg);
    if (AccountDbContext::Insert(account)) {
         send(msg, CER_ACCOUNT_EXIST);
    }
    return 0;
}

int  Server::logon(LOGON_REQUEST *msg) {
    _HANLE_DEBUG("Log on");
    account = AccountDbContext::FindByFirstNameAndPIN(msg->get_FirstName(), msg->get_PIN());
    if (account == NULL) {
        send(msg, CER_INVALID_NAME_PIN);
        return 1;
    }
    return 0;
}

int  Server::logoff(LOGOFF_REQUEST *msg) {
    (void)*msg;
    delete account;
    account = NULL;
    stop = 1;
    return 0;
}

int  Server::deposit(DEPOSIT_REQUEST *msg) {
    std::cout << "Handle deposit " << std::endl;
    if (msg->get_Amount() == 0) {
        send(msg, CER_INVALID_ARGUMENTS);
        return 1;
    }

    if ((msg->get_DepositType() != DT_CHECK && msg->get_DepositType() != DT_CASH)) {
        send(msg, CER_INVALID_ARGUMENTS);
        return 1;
    }

    if (AccountDbContext::Deposit(account->GetID(), msg->get_Amount()) == 0) {
        send(msg, CER_ERROR);
        return 1;
    }
    return 0;
}

int  Server::withdrawl(WITHDRAWAL_REQUEST *msg) {
    if (msg->get_Amount() == 0) {
        send(msg, CER_INVALID_ARGUMENTS);
        return 1;
    }

    if (AccountDbContext::Withdrawal(account->GetID(), msg->get_Amount()) == 0) {
        send(msg, CER_INSUFFICIENT_FUNDS);
        return 1;
    }

    return 0;
}

int Server::purchaseStamps(STAMP_PURCHASE_REQUEST *msg) {
    if (msg->get_Amount() == 0) {
        send(msg, CER_INVALID_ARGUMENTS);
        return 1;
    }
    if (AccountDbContext::Withdrawal(account->GetID(), msg->get_Amount()) == 0) {
        send(msg, CER_INSUFFICIENT_FUNDS);
        return 1;
    }
    return 0;
}

int Server::queryBalance(BALANCE_QUERY_REQUEST *msg) {
    DWORD Balance;
    if (AccountDbContext::QueryBalance(account->GetID(), &Balance) == 0) {
        send(msg, CER_ERROR);
    } else {
        send(msg, CER_SUCCESS, Balance);
    }
    return 1;
}

int Server::queryTransaction(TRANSACTION_QUERY_REQUEST *msg) {
    if (msg->get_TransactionCount() == 0) {
        send(msg, CER_INVALID_ARGUMENTS);
        return 1;
    }

    // TODO: get the array of transactions
    DWORD Count;
    TransactionLog *t = TransactionLogDbContext::FindRange
            (account->GetID(), msg->get_StartID(), msg->get_TransactionCount(), &Count);
    send(msg, CER_SUCCESS, Count, t);
    return 0;
}
