#include "Client.h"
#include <iostream>

using namespace cnp;
using namespace net;
using namespace std;

Client::~Client () {

}

#define DEBUG

Message *Client::sendAndReceive(void *data) {
    Message *req = (Message*)data;;
#ifdef DEBUG
    req->printAsRequest();
#endif
    send(req);
    Message *resp = recv();
#ifdef DEBUG
    resp->printAsResponse();
#endif

    if (req->get_MsgType() != MT_CONNECT_REQUEST_ID
            && resp->get_ClientID() != req->get_ClientID()) {
        throw Exception("Mismatch Client");
    }

    if (resp->get_Sequence() != req->get_Sequence()) {
        throw Exception("Mismatch Sequence");
    }

    if (resp->upperType() != req->upperType()) {
        std::cout << (resp->get_MsgType() & 0xFF) << " == " << (req->get_MsgType() & 0xFF) << std::endl;
        throw Exception ("Upper Type");
    }

    return resp;
}



CONNECT_RESPONSE *Client::connect() {
    CONNECT_REQUEST request;
    CONNECT_RESPONSE *response = (CONNECT_RESPONSE*) sendAndReceive(&request);
    ClientId = response->get_ClientID();
    return response;
}

CREATE_ACCOUNT_RESPONSE* Client::createAccount(const char* FirstName, const char *LastName, const char *Email, WORD randomPIN) {
    CREATE_ACCOUNT_REQUEST r(ClientId, FirstName, LastName, Email, randomPIN);
    return (CREATE_ACCOUNT_RESPONSE*)sendAndReceive(&r);
}

LOGON_RESPONSE* Client::logon(const char* FirstName, WORD PIN) {
    LOGON_REQUEST r(ClientId, FirstName, PIN);
    return (LOGON_RESPONSE*)sendAndReceive(&r);
}

LOGOFF_RESPONSE *Client::logoff() {
    LOGOFF_REQUEST r(ClientId);
    return (LOGOFF_RESPONSE*)sendAndReceive(&r);
}

DEPOSIT_RESPONSE *Client::deposit(DWORD Amount) {
    DEPOSIT_REQUEST r(ClientId, Amount, DT_CHECK);
    return (DEPOSIT_RESPONSE*)sendAndReceive(&r);
}

WITHDRAWAL_RESPONSE *Client::withdraw(DWORD Amount) {
    WITHDRAWAL_REQUEST r(ClientId, Amount);
    return (WITHDRAWAL_RESPONSE*)sendAndReceive(&r);
}

STAMP_PURCHASE_RESPONSE *Client::purchaseStamps(DWORD Amount) {
    STAMP_PURCHASE_REQUEST r(ClientId, Amount);
    return (STAMP_PURCHASE_RESPONSE*) sendAndReceive(&r);
}

BALANCE_QUERY_RESPONSE *Client::queryBalance() {
    BALANCE_QUERY_REQUEST r(ClientId);
    BALANCE_QUERY_RESPONSE *b = (BALANCE_QUERY_RESPONSE*)sendAndReceive(&r);
    return b;
}

TRANSACTION_QUERY_RESPONSE *Client::queryTransaction(DWORD Offset, DWORD Limit) {
    TRANSACTION_QUERY_REQUEST r(ClientId, Offset, Limit);
    return (TRANSACTION_QUERY_RESPONSE*)sendAndReceive(&r);
}


