TEMPLATE = subdirs
CONFIG += console
CONFIG += ordered
CONFIG -= app_bundle
CONFIG -= qt

SUBDIRS = Server \
          Test \
    Client

OBJECTS_DIR = ./build

OTHER_FILES += README \
    qmake_to_Makefile.sh \
    distribute.sh

# SQLite

DEFINES += THREADSAFE=1
DEFINES += -DDEBUG
