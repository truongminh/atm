TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

TARGET = ../bin/Client # move the executable to the build folder
OBJECTS_DIR = ../build

SOURCES += \
    ../net/Socket.cpp \
    ../model/DbContext.cpp \
    ../model/account.cpp \
    ../sqlite3/Transaction.cpp \
    ../sqlite3/Statement.cpp \
    ../sqlite3/sqlite3.c \
    ../sqlite3/Database.cpp \
    ../sqlite3/Column.cpp \
    ../net/CnpSocket.cpp \
    ../api/Client.cpp \
    ../model/TransactionLog.cpp \
    ../net/Message.cpp \
    main_client.cpp

HEADERS += \
    ../Custom_Protocol.h \
    ../net/Message.h \
    ../net/CNP_Protocol.h \
    ../net/Socket.h \
    ../model/DbContext.h \
    ../model/account.h \
    ../sqlite3/Transaction.h \
    ../sqlite3/Statement.h \
    ../sqlite3/SQLiteCpp.h \
    ../sqlite3/sqlite3.h \
    ../sqlite3/Exception.h \
    ../sqlite3/Database.h \
    ../sqlite3/Column.h \
    ../sqlite3/Assertion.h \
    ../net/CnpSocket.h \
    ../api/Client.h \
    ../model/TransactionLog.h

LIBS += -lpthread -ldl
INCLUDEPATH += ../
