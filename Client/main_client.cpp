#include <iostream>
#include "api/Client.h"

using namespace std;
using namespace net;

static Client *c;
static Account *a;

void init(string &address, int port) {
    c = new Client(address, port);
    c->connect();
    a = NULL;
}

/************ Declaration of Helpers *******************/

void print_main_menu();

/************* Option 1. Create a new account ***************/
void CreateAccount() {
    string FirstName;
    string LastName;
    string EmailAddress;
    WORD PIN;
    DWORD SSNumber;
    DWORD DLNumber;
    cout << "Enter Firstname: ";
    cin >> FirstName;
    cout << "Enter Lastname: ";
    cin >> LastName;
    cout << "Enter Email: ";
    cin >> EmailAddress;
    cout << "Enter PIN:";
    cin >> PIN;
    cout << "Enter SS Number:";
    cin >> SSNumber;
    cout << "Enter DL Number:";
    cin >> DLNumber;

    DWORD res = (c->createAccount(FirstName.c_str(), LastName.c_str(), EmailAddress.c_str(), PIN))->get_ResponseResult();
    if (res == CER_SUCCESS) {

        cout << "New account has Firstname = " << FirstName << " and PIN = " << PIN << endl;


    } else {
        if (res == CER_ACCOUNT_EXIST) {
            cout << "Account existed Firstname = " << FirstName << " and PIN = " << PIN << endl;
        } else {
            cout << "Unknown error" << endl;
        }
    }
}

int notLoggedOn() {
    if (a == NULL) {
        cout << "You have not logged on." << endl;
        return 1;
    }
    return 0;
}

#define REQUIRE_LOG_ON do { if (notLoggedOn()) { return; }  } while(0);


int isFail() {
    if (std::cin.fail()) {
        std::cout << endl << "Invalid input must be integer" << std::endl;
        std::cin.clear();
        std::cin.ignore();
        return 1;
    }
    return 0;
}

#define CHECK_FAIL do {if(isFail()) return;} while(0);

/************** Option 2. Log on **************/
void LogOn() {
    string FirstName;
    WORD PIN;
    cout << "Enter Firstname: ";
    cin >> FirstName;
    cout << "Enter PIN:";
    cin >> PIN;

    CHECK_FAIL;

    DWORD res = (c->logon(FirstName.c_str(), PIN))->get_ResponseResult();
    if (res == CER_SUCCESS) {
        if (a != NULL) {
            delete a;
        }
        cout << "Logon as Firstname = " << FirstName << endl;
        a = new Account(FirstName.c_str(), PIN);
    } else {
        if (res == CER_INVALID_NAME_PIN) {
            cout << "Account not exist"<< endl;
        } else {
            cout << "Unknown error" << endl;
        }
    }
}

/*********** Option 3. Deposit **************/
void Deposit() {
    REQUIRE_LOG_ON
    DWORD Amount;
    double inAmount;
    cout << "Enter Amount: $";
    cin >> inAmount;
    Amount = (DWORD)(inAmount * 100);
    DWORD res = (c->deposit(Amount)->get_ResponseResult());
    if (res == CER_SUCCESS) {
        cout << "Deposited " << inAmount << endl;
    } else {
        if (res == CER_INVALID_NAME_PIN) {
            cout << "Invalid Name and PIN pair"<< endl;
        } else {
            cout << "Unknown error" << endl;
        }
    }
}

/************ Option 4: Withdraw *********************/
void Withdraw() {
    REQUIRE_LOG_ON
    DWORD Amount;
    double inAmount;
    cout << "Enter Amount: $";
    cin >> inAmount;
    Amount = (DWORD)(inAmount * 100);
    DWORD res = (c->withdraw(Amount)->get_ResponseResult());
    if (res == CER_SUCCESS) {
        cout << "Withdrew " << inAmount << endl;
    } else {
        if (res == CER_INSUFFICIENT_FUNDS) {
            cout << "Insufficient fund"<< endl;
        } else {
            cout << "Unknown error" << endl;
        }
    }
}

/*********** Option 5: Query Balance ********/
void QueryBalance() {
    REQUIRE_LOG_ON
    BALANCE_QUERY_RESPONSE *r = c->queryBalance();
    DWORD res = r->get_ResponseResult();
    DWORD Balance = r->get_Balance();
    double outBalance = (double)Balance / 100;
    if (res == CER_SUCCESS) {
        cout << "Current balance: $" << outBalance << endl;
    } else {
        cout << "Unknown error" << endl;
    }
    delete r;
}

void printTransaction(const cnp::TRANSACTION *t) {
    std::cout << "\tAt: " << t->m_qwDateTime
              << "\tAmmount: " << ((t->m_wType == TT_DEPOSIT)? "+" : "-")
              << t->m_dwAmount
              << std::endl;
}

/************ Option 6: Query Transaction *********************/
void QueryTransaction() {
    REQUIRE_LOG_ON;
    DWORD Offset;
    DWORD Limit;
    cout << "Start: ";
    cin >> Offset;
    cout << "Count: ";
    cin >> Limit;
    TRANSACTION_QUERY_RESPONSE *r = c->queryTransaction(Offset, Limit);
    if (r->get_ResponseResult() != CER_SUCCESS) {
        cout << "Unknown error" << endl;
        delete r;
        return;
    }
    const cnp::TRANSACTION *t = r->get_Transaction();
    for (DWORD i = 0; i < r->get_Count(); ++i) {
        printTransaction(t+i);
    }
}

/*********** Option 7: Purchase Stamps  ********/
void PurchaseStamps() {
    REQUIRE_LOG_ON
    DWORD Amount;
    double inAmount;
    cout << "Enter Amount: $";
    cin >> inAmount;
    Amount = (DWORD)(inAmount * 100);
    DWORD res = (c->withdraw(Amount)->get_ResponseResult());
    if (res == CER_SUCCESS) {
        cout << "Purchase Stamps " << inAmount << endl;
    } else {
        if (res == CER_INSUFFICIENT_FUNDS) {
            cout << "Insufficient fund"<< endl;
        } else {
            cout << "Unknown error" << endl;
        }
    }
}

/************ Option 8: Log off*********************/
void Logoff() {
    REQUIRE_LOG_ON
    LOGOFF_RESPONSE *r = c->logoff();
    DWORD res = r->get_ResponseResult();
    if (res == CER_SUCCESS) {
        cout << "You have been logged off. " << endl;
    } else {
        cout << "Unknown error" << endl;
    }
    delete r;
    delete a;
    a = NULL;
}

/*******************************************************************************************/
int main(int argc, char *argv[]) {
    if (argc !=  3) {     // Test for correct number of arguments
        cerr << "Usage: " << argv[0]
             << " <Server> [<Server Port>]" << endl;
        exit(1);
    }

    string address = argv[1]; // First arg: server address
    unsigned short port = atoi(argv[2]);

    init(address, port);

    int choice;
    int stop = 0;
    while (!stop) {
        print_main_menu();
        cin >> choice;
        if (isFail()) {
            continue;
        }

        switch (choice) {
        case 1:
            CreateAccount();
            break;
        case 2:
            LogOn();
            break;
        case 3:
            Deposit();
            break;
        case 4:
            Withdraw();
            break;
        case 5:
            QueryBalance();
            break;
        case 6:
            QueryTransaction();
            break;
        case 7:
            PurchaseStamps();
        case 8:
            Logoff();
            break;
        case 9:
            stop = 1;
            break;
        default:
            cout << endl << "Please enter a number between 1 and 9" << endl;
            break;
        }
    }

    return 0;
}



/************ Helpers *******************/


void print_main_menu() {
    cout << "========= MAIN MENU ==========\n";
    if (notLoggedOn()) {
        cout << "1. Create a new client.\n"
             << "2. Logon.\n"
             << "9. Exit\n";
    } else {
        cout << "3. Deposit.\n"
             << "4. Withdraw.\n"
             << "5. Query Balance.\n"
             << "6. Query Transaction\n"
             << "7. Purchase Stamp\n"
             << "8. Logoff\n"
             << "9. Exit\n";
    }

    cout << "Please enter one option: ";
}
