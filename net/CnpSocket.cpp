
#include "Socket.h"
#include <cstring>
#include <cstdlib>
#include <cstdio>

#ifdef WIN32
  #include <winsock.h>         // For socket(), connect(), send(), and recv()
  typedef int socklen_t;
  typedef char raw_type;       // Type used for raw data on this platform
#else
  #include <sys/types.h>       // For data types
  #include <sys/socket.h>      // For socket(), connect(), send(), and recv()
  #include <netdb.h>           // For gethostbyname()
  #include <arpa/inet.h>       // For inet_addr()
  #include <unistd.h>          // For close()
  #include <netinet/in.h>      // For sockaddr_in
  typedef void raw_type;       // Type used for raw data on this platform
#endif

#include <errno.h>             // For errno
// TCPSocket Code

#include "CnpSocket.h"
#include <iostream>

namespace net {

int CnpSocket::readFully(char* buf, int len) {
    int pending = len;
    while (pending > 0) {
        len = recv(buf, pending);
        buf += len;
        pending -= len;
    }
    return pending;
}

void CnpSocket::writeFully(char* buf, int len) {
    int pending = len;
    while (pending > 0) {
        len = send(buf, pending);
        buf += len;
        pending -= len;

    }
}

Message *CnpSocket::readMessage() throw(SocketException) {
    Message *msg = new Message;
    // pending bytes
    if (readFully(( char*) msg, Message::HeaderLength)) {
        throw SocketException("Cannot read header");
    }
    char* buffer = new char[msg->getSize()];
    memcpy(buffer, (char*)msg, Message::HeaderLength);
    if (readFully(buffer + Message::HeaderLength, msg->bodyLength())) {
        throw SocketException("Cannot read body");
    }
    return (Message*)buffer;
}


void CnpSocket::writeMessage(Message *message) {
    writeFully((char*)message, message->getSize());
}

CnpSocket::CnpSocket()
    throw(SocketException) : CommunicatingSocket(SOCK_STREAM,
    IPPROTO_TCP) {
}

CnpSocket::CnpSocket(const string &foreignAddress, unsigned short foreignPort)
    throw(SocketException) : CommunicatingSocket(SOCK_STREAM, IPPROTO_TCP) {
  connect(foreignAddress, foreignPort);
}

CnpSocket::CnpSocket(int newConnSD) : CommunicatingSocket(newConnSD) {
}

// TCPServerSocket Code

TCPServerSocket::TCPServerSocket(unsigned short localPort, int queueLen)
    throw(SocketException) : Socket(SOCK_STREAM, IPPROTO_TCP) {
  setLocalPort(localPort);
  setListen(queueLen);
}

TCPServerSocket::TCPServerSocket(const string &localAddress,
    unsigned short localPort, int queueLen)
    throw(SocketException) : Socket(SOCK_STREAM, IPPROTO_TCP) {
  setLocalAddressAndPort(localAddress, localPort);
  setListen(queueLen);
}

CnpSocket *TCPServerSocket::accept() throw(SocketException) {
  int newConnSD;
  if ((newConnSD = ::accept(sockDesc, NULL, 0)) < 0) {
    throw SocketException("Accept failed (accept())", true);
  }

  return new CnpSocket(newConnSD);
}

void TCPServerSocket::setListen(int queueLen) throw(SocketException) {
  if (listen(sockDesc, queueLen) < 0) {
    throw SocketException("Set listening socket failed (listen())", true);
  }
}

}
