/*
 * Message.h
 *
 *  Created on: Mar 30, 2015
 *      Author: thienlong
 */

#ifndef MESSAGE_H_
#define MESSAGE_H_

#include "net/CNP_Protocol.h"
#include <exception>

using namespace cnp;

namespace net {

/**
 *   Signals a problem with the message
 */
class MessageException : public std::exception {
public:
  /**
   *   Construct a SocketException with a explanatory message.
   *   @param message explanatory message
   *   @param incSysMsg true if system message (from strerror(errno))
   *   should be postfixed to the user provided message
   */
    MessageException(CER_TYPE err) throw() : cer(err) {};

  /**
   *   Provided just to guarantee that no exceptions are thrown.
   */
  ~MessageException() throw() {} ;

  CER_TYPE getError() {
      return cer;
  };

private:
  CER_TYPE cer;
};

#pragma pack(push, 1)

typedef struct Message : cnp::STD_HDR {
    static const int HeaderLength = sizeof(STD_HDR);
    WORD upperType() {
        return get_MsgType() & 0xFFFF; // low bytes
    }

    int getSize() {return HeaderLength + m_wDataLen;};
    int bodyLength() { return m_wDataLen; };
    void isValidRequest() throw(MessageException);
    void isValidResponse() throw(MessageException);
    void checkClient(DWORD ClienID) throw (MessageException);
    int requireLoggedOn();

    void printAsRequest();
    void printAsResponse();
    Message() : STD_HDR() {};
    Message *Response(DWORD err = CER_SUCCESS, DWORD argc = 0, void *argv = NULL);
} Message;

#pragma pack(pop)






} /* namespace net */

#endif /* MESSAGE_H_ */
