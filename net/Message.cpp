#include "Message.h"
#include <iostream>

namespace net {

void Message::isValidRequest() throw(MessageException) {
    switch(get_MsgType()) {
    case MT_CONNECT_REQUEST_ID:
    case MT_CREATE_ACCOUNT_REQUEST_ID:
    case MT_LOGON_REQUEST_ID:
    case MT_LOGOFF_REQUEST_ID:
    case MT_DEPOSIT_REQUEST_ID:
    case MT_WITHDRAWAL_REQUEST_ID:
    case MT_BALANCE_QUERY_REQUEST_ID:
    case MT_TRANSACTION_QUERY_REQUEST_ID:
    case MT_PURCHASE_STAMPS_REQUEST_ID:
        break;
    default:
        throw MessageException(CER_UNSUPPORTED_PROTOCOL);

    }
}

void Message::checkClient(DWORD ClienID) throw (MessageException) {
    if (get_ClientID() != ClienID) {
        throw MessageException(CER_INVALID_CLIENTID);
    }
}

int Message::requireLoggedOn() {
    DWORD msgType = get_MsgType();
    return (msgType != MT_CONNECT_REQUEST_ID) &&
            (msgType != MT_CREATE_ACCOUNT_REQUEST_ID) &&
            (msgType != MT_LOGON_REQUEST_ID);
}

void Message::printAsRequest() {
    std::cout << "------ Request ------" << std::endl;
    std::cout << "Thread: " << pthread_self() << std::endl;
    std::cout << "Client: " << get_ClientID() << std::endl;
    std::cout << "Sequence: " << get_Sequence() << std::endl;
    switch(get_MsgType()) {
    case MT_CONNECT_REQUEST_ID:
        std::cout << "Method: " << "Connect" << std::endl;
        break;
    case MT_CREATE_ACCOUNT_REQUEST_ID:
        std::cout << "Method: " << "Create Account" << std::endl;
        break;
    case MT_LOGON_REQUEST_ID:
        std::cout << "Method: " << "Log on" << std::endl;
        break;
    case MT_LOGOFF_REQUEST_ID:
        std::cout << "Method: " << "Log off" << std::endl;
        break;
    case MT_DEPOSIT_REQUEST_ID:
        std::cout << "Method: " << "Deposit" << std::endl;
        break;
    case MT_WITHDRAWAL_REQUEST_ID:
        std::cout << "Method: " << "Withdrawwal" << std::endl;
        break;
    case MT_BALANCE_QUERY_REQUEST_ID:
        std::cout << "Method: " << "Balance" << std::endl;
        break;
    case MT_TRANSACTION_QUERY_REQUEST_ID:
        std::cout << "Method: " << "Trasaction Query" << std::endl;
        break;
    case MT_PURCHASE_STAMPS_REQUEST_ID:
        std::cout << "Method: " << "Purchase Stamps" << std::endl;
        break;
    default:
        std::cout << "Method: " << "Unknown" << std::endl;
        break;
    }
}

#define _status(type) do{ std::cout << "Status: " << ((type*)this)->get_ResponseResult() << std::endl; } while(0)

void Message::printAsResponse() {
    std::cout << "------ Response ------" << std::endl;
    std::cout << "Thread: " << pthread_self() << std::endl;
    std::cout << "Client: " << get_ClientID() << std::endl;
    std::cout << "Sequence: " << get_Sequence() << std::endl;
    switch(get_MsgType()) {
    case MT_CONNECT_RESPONSE_ID:
        std::cout << "Method: " << "Connect" << std::endl;
        _status(CONNECT_RESPONSE);
        break;
    case MT_CREATE_ACCOUNT_RESPONSE_ID:
        std::cout << "Method: " << "Create Account" << std::endl;
        _status(CREATE_ACCOUNT_RESPONSE);
        break;
    case MT_LOGON_RESPONSE_ID:
        std::cout << "Method: " << "Log on" << std::endl;
        _status(LOGON_RESPONSE);
        break;
    case MT_LOGOFF_RESPONSE_ID:
        std::cout << "Method: " << "Log off" << std::endl;
        _status(LOGOFF_RESPONSE);
        break;
    case MT_DEPOSIT_RESPONSE_ID:
        std::cout << "Method: " << "Deposit" << std::endl;
        _status(DEPOSIT_RESPONSE);
        break;
    case MT_WITHDRAWAL_RESPONSE_ID:
        std::cout << "Method: " << "Withdrawwal" << std::endl;
        _status(WITHDRAWAL_RESPONSE);
        break;
    case MT_BALANCE_QUERY_RESPONSE_ID:
        std::cout << "Method: " << "Balance" << std::endl;
        _status(BALANCE_QUERY_RESPONSE);
        break;
    case MT_TRANSACTION_QUERY_RESPONSE_ID:
        do {
            std::cout << "Method: " << "Trasaction Query" << std::endl;
            _status(TRANSACTION_QUERY_RESPONSE);
            TRANSACTION_QUERY_RESPONSE *r = (TRANSACTION_QUERY_RESPONSE*)this;
            const cnp::TRANSACTION *t = r->get_Transaction();
            for (DWORD i = 0; i < r->get_Count(); ++i) {
                std::cout << "Account: " << t[i].m_dwID
                          << "\tTimestamp: " << t[i].m_qwDateTime
                          << "\tAmmount: " << ((t[i].m_wType == TT_DEPOSIT)? "+" : "-")
                          << t[i].m_dwAmount
                          << std::endl;
            }

        } while(0);
        break;
    case MT_PURCHASE_STAMPS_RESPONSE_ID:
        std::cout << "Method: " << "Purchase Stamps" << std::endl;
        _status(STAMP_PURCHASE_RESPONSE);
        break;
    default:
        std::cout << "Method: " << "Unknown" << std::endl;
        break;
    }
}

#define _create_message(type, resp, err)  do { \
    resp = (Message*) new type (err, get_ClientID(), get_Sequence(), get_Context()); \
} while(0)


Message *Message::Response(DWORD err, DWORD argc, void *argv) {
    Message *resp = NULL;
    switch(get_MsgType()) {
    case MT_CREATE_ACCOUNT_REQUEST_ID:
        _create_message(CREATE_ACCOUNT_RESPONSE, resp, err);
        break;

    case MT_LOGON_REQUEST_ID:
        _create_message(LOGON_RESPONSE, resp, err);
        break;

    case MT_LOGOFF_REQUEST_ID:
        _create_message(LOGOFF_RESPONSE, resp, err);
        break;

    case MT_DEPOSIT_REQUEST_ID:
        _create_message(DEPOSIT_RESPONSE, resp, err);
        break;

    case MT_WITHDRAWAL_REQUEST_ID:
        _create_message(WITHDRAWAL_RESPONSE, resp, err);
        break;

    case MT_BALANCE_QUERY_REQUEST_ID:
        do {
            DWORD balance = (DWORD)argc;
            resp = (Message*) new BALANCE_QUERY_RESPONSE(err, get_ClientID(), balance,  get_Sequence(), get_Context());

        } while(0);
        break;

    case MT_TRANSACTION_QUERY_REQUEST_ID:
        do {
            DWORD count = (DWORD)argc;
            TRANSACTION *transactions = (TRANSACTION*)argv;
            if (count == 0) {
                resp = (Message*)new TRANSACTION_QUERY_RESPONSE
                        (CER_SUCCESS, get_ClientID(), count, get_Sequence(), get_Context() );
            } else {
                resp = (Message*)TRANSACTION_QUERY_RESPONSE::make
                        (err, get_ClientID(), count, get_Sequence(), get_Context(), transactions);
            }
        } while(0);
        break;

    case MT_PURCHASE_STAMPS_REQUEST_ID:
        _create_message(STAMP_PURCHASE_RESPONSE, resp, err);
        break;

    case MT_CONNECT_REQUEST_ID:
        // fall through
    default:
        do {
            // messgae has no client id on connect
            DWORD clientID = (DWORD)argc;
            resp = (Message*) new CONNECT_RESPONSE(CER_SUCCESS, clientID, g_wMajorVersion, g_wMinorVersion, get_Sequence(), get_Context());
        } while(0);
        break;
    }


    return resp;
}

}
