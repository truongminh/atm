#!/bin/sh
ZIP_FILE="ATM.zip"
rm ATM.zip
./qmake.sh
zip -r $ZIP_FILE . -i *.[hc]
zip -r $ZIP_FILE . -i *.cpp
zip -r $ZIP_FILE . -i Makefile
zip -r $ZIP_FILE . -i */Makefile
# zip -r $ZIP_FILE . -i *.sh
zip $ZIP_FILE 	      README
