/*
 *   C++ sockets on Unix and Windows
 *   Copyright (C) 2002
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <iostream>           // For cerr and cout
#include <vector>
#include <assert.h>
#include <string>

#include "api/Client.h"

using namespace std;
using namespace net;

static char *rand_string(char *str, size_t size)
{
    const char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJK...";
    if (size) {
        --size;
        for (size_t n = 0; n < size; n++) {
            int key = rand() % (int) (sizeof charset - 1);
            str[n] = charset[key];
        }
        str[size] = '\0';
    }
    return str;
}

#define _assert_status(x, s) assert((x->get_ResponseResult()) == s)
#define _assert_success(x) _assert_status((x), CER_SUCCESS)
#define _assert_balance(x, b) do { \
        _assert_success(x); \
        assert(x->get_Balance() == b); \
    }while(0);

void *ThreadHandler(void *client) {
    // Guarantees that thread resources are deallocated upon return
    pthread_detach(pthread_self());

    srand(time(NULL) ^ pthread_self());
    Client *c = (Client*)client;
    _assert_success(c->connect());
    char FirstName[128];
    rand_string(FirstName, 8);
    WORD PIN = (WORD)random();
    try {
        std::cout << FirstName << PIN << std::endl;
        _assert_success(c->createAccount(FirstName, "Lastname", "Email", PIN));
        _assert_status(c->createAccount(FirstName, "Lastname", "Email", PIN), CER_ACCOUNT_EXIST);
        _assert_success(c->logon(FirstName, PIN));
        _assert_success(c->deposit(100));
        _assert_balance(c->queryBalance(), 100);
        _assert_success(c->withdraw(100));
        _assert_balance(c->queryBalance(), 0);
        _assert_status(c->purchaseStamps(1), CER_INSUFFICIENT_FUNDS);
        _assert_balance(c->queryBalance(), 0);
        c->queryTransaction(0, 10);
        _assert_success(c->logoff());
        delete c;
    } catch (Exception &e) {
        std::cout << "Client: " << e.what() << " exit" << pthread_self() << std::endl;
        sleep(1);
    }

    pthread_exit(NULL);
    return NULL;
}

pthread_t make_client(void *client) {
    // Create client thread
    pthread_t threadID;
    try {

        if (pthread_create(&threadID, NULL, ThreadHandler, client) != 0) {
            cerr << "Unable to create thread" << endl;
            exit(1);
        }

    } catch(SocketException &e) {
        cerr << e.what() << endl;
        exit(1);
    }

    return threadID;
}

#define NUM_THREAD 1

int main(int argc, char *argv[]) {
    if (argc !=  3) {     // Test for correct number of arguments
        cerr << "Usage: " << argv[0]
             << " <Server> [<Server Port>]" << endl;
        exit(1);
    }

    string address = argv[1]; // First arg: server address
    unsigned short port = atoi(argv[2]);
    pthread_t threads[NUM_THREAD];


    for (int i = 0; i < NUM_THREAD; ++i) {
        threads[i] = make_client((void*) new Client(address, port));
    }

    for (int i = 0; i < NUM_THREAD; ++i) {
        pthread_join(threads[i], NULL);
        std::cout << "join " << i << std::endl;
    }

    return 0;
}
