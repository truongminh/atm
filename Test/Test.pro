TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle

CONFIG -= qt

TARGET = ../bin/Test
OBJECTS_DIR = ../build

SOURCES += \
    ../model/DbContext.cpp \
    ../model/account.cpp \
    ../net/Socket.cpp \
    ../sqlite3/Transaction.cpp \
    ../sqlite3/Statement.cpp \
    ../sqlite3/sqlite3.c \
    ../sqlite3/Database.cpp \
    ../sqlite3/Column.cpp \
    ../api/Client.cpp \
    ../net/CnpSocket.cpp \
    ../model/TransactionLog.cpp \
    ../net/Message.cpp \
    Test.cpp

HEADERS += \
    ../model/DbContext.h \
    ../model/account.h \
    ../net/Socket.h \
    ../net/Message.h \
    ../net/CNP_Protocol.h \
    ../sqlite3/Transaction.h \
    ../sqlite3/Statement.h \
    ../sqlite3/SQLiteCpp.h \
    ../sqlite3/sqlite3.h \
    ../sqlite3/Exception.h \
    ../sqlite3/Database.h \
    ../sqlite3/Column.h \
    ../sqlite3/Assertion.h \
    ../api/Client.h \
    ../net/CnpSocket.h \
    ../model/TransactionLog.h


INCLUDEPATH += ..

LIBS += -lpthread
LIBS += -ldl
