#include "TransactionLog.h"
#include <sstream>
#include <iostream>

namespace model {

void TransactionLog::print()  {
    std::cout
        << "AccountID: " << getAccountID() << std::endl
        << "Timestamp: " << getTimestamp() << std::endl
        << "Amount: " << getAmount() << std::endl
        << "Type: " << getType() << std::endl;

}


std::string TransactionLogDbContext::CreateTable() {
    std::ostringstream stringStream;
    // TODO: QWORD Timestamp
    stringStream
        << "CREATE TABLE IF NOT EXISTS TransactionLog "
        << "("
            << "ID INTEGER PRIMARY KEY, "
            << "AccountID INTEGER NOT NULL REFERENCES ACCOUNT(ID), "
            << "Timestamp INT8 NOT NULL, "
            << "Amount INT NOT NULL CHECK(Amount > 0), "
            << "Type SMALLINT NOT NULL "
        << ");";
    return stringStream.str();
}

static TransactionLogDbContext *context = NULL;


TransactionLogDbContext *TransactionLogDbContext::Get() {
    if (context == NULL) {
        context = new TransactionLogDbContext();
    }
    return context;
}

TransactionLogDbContext::TransactionLogDbContext()  : db(DbContext::Get()) {

    db->exec(TransactionLogDbContext::CreateTable());
    insertTransactionLog = db->createStatement("INSERT INTO TransactionLog(AccountID, Timestamp, Amount, Type)"
                                           "VALUES(:AccountID, :Timestamp, :Amount, :Type);");
    // bind: AccountID, Limit and Offset
    findRange = db->createStatement(
                "SELECT AccountID, Amount, Type, Timestamp "
                "FROM TRANSACTIONLOG "
                "WHERE AccountID = :AccountID "
                "ORDER BY ID DESC "
                "LIMIT :Limit OFFSET :Offset;");
}

DWORD TransactionLogDbContext::Insert(TransactionLog *t) {
    SQLite::Statement *query = Get()->insertTransactionLog;
    query->bind(":AccountID", (const int) t->getAccountID());
    query->bind(":Timestamp", (const sqlite3_int64) t->getTimestamp());
    query->bind(":Amount", (const int) t->getAmount());
    query->bind(":Type", (const int) t->getType());
    DWORD affected = query->exec();
    query->reset();
    return affected;
}

DWORD TransactionLogDbContext::Deposit(DWORD AccountID, DWORD Amount) {
    TransactionLog t(AccountID, Amount, TT_DEPOSIT);
    return Insert(&t);
}

DWORD TransactionLogDbContext::Withdrawal(DWORD AccountID, DWORD Amount) {
    TransactionLog t(AccountID, Amount, TT_WITHDRAWAL);
    return Insert(&t);
}


TransactionLog* TransactionLogDbContext::FindRange(DWORD AccountID, DWORD Offset, DWORD Limit, DWORD *Count) {
    char *buf = new char[Limit * sizeof(TransactionLog)];
    TransactionLog *t = (TransactionLog*)buf;
    DWORD i = 0;
    SQLite::Statement *query = Get()->findRange;
    query->bind(":AccountID", (const int)AccountID);
    query->bind(":Limit", (const int) Limit);
    query->bind(":Offset", (const int) Offset);
    while(query->executeStep()) {
        std::cout << "Type " << query->getColumn(2).getInt();
        t[i] = TransactionLog(query->getColumn(0).getInt(),
                              query->getColumn(1).getInt(),
                              query->getColumn(2).getInt(),
                              query->getColumn(3).getInt64());
        i++;
    }
    query->reset();
    *Count = i;
    return t;
}

}
