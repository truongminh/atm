
#include <iostream>
#include <string>
#include <cstdio>

#include <sstream>
#include <stdio.h>
#include "account.h"
#include "TransactionLog.h"
#include "sqlite3/Transaction.h"

namespace model {
    void Account::print()  {
        std::cout
            << "ID: " << ID << std::endl
            << "First Name: " <<  get_FirstName() << std::endl
            << "Last Name: " << get_LastName() << std::endl
            << "Email Address: " << get_EmailAddress() << std::endl
            << "PIN: " << get_PIN() << std::endl
            << "SSNumber: " << get_SSNumber() << std::endl
            << "DLNumber: " << get_DLNumber() << std::endl
            << "Balance: " << Balance << std::endl;

    }


    // TODO: CHECK Balance < 0
    std::string AccountDbContext::CreateTable() {
        std::ostringstream stringStream;
        stringStream
            << "CREATE TABLE IF NOT EXISTS ACCOUNT "
            << "("
                << "ID INTEGER PRIMARY KEY, "
                << "FirstName CHAR(" << MAX_NAME_LEN << ") NOT NULL, "
                << "LastName CHAR(" << MAX_NAME_LEN << ") NOT NULL, "
                << "EmailAddress CHAR(" << MAX_NAME_LEN << ") NOT NULL, "
                << "PIN INT NOT NULL UNIQUE, "
                << "SSNumber INT, "
                << "DLNumber INT, "
                << "Balance INT DEFAULT 0 NOT NULL CHECK (Balance >= 0) "
            << ");";
        return stringStream.str();
    }

    static AccountDbContext *context = NULL;

    AccountDbContext *AccountDbContext::Get() {
        if (context == NULL) {
            context = new AccountDbContext();
        }
        return context;
    }

    AccountDbContext::AccountDbContext() : db(DbContext::Get()){

        db->exec(AccountDbContext::CreateTable());
        insertAccount = db->createStatement("INSERT INTO ACCOUNT(FirstName, LastName, EmailAddress, PIN, SSNumber, DLNumber)"
                                  "VALUES(:FirstName, :LastName, :EmailAddress, :PIN, :SSNumber, :DLNumber);");
        findById = db->createStatement(
                    "SELECT ID, FirstName, LastName, EmailAddress, PIN, SSNumber, DLNumber "
                    "FROM ACCOUNT "
                    "WHERE ID = :id;");
        findByFirstNameAndPin = db->createStatement(
                    "SELECT ID, FirstName, LastName, EmailAddress, PIN, SSNumber, DLNumber "
                    "FROM ACCOUNT "
                    "WHERE FirstName = :FirstName AND PIN = :PIN;");
        m_prepare_deposit = db->createStatement("UPDATE ACCOUNT SET Balance = Balance + :Amount WHERE ID = :ID;");
        m_prepare_widthdrawal = db->createStatement("UPDATE ACCOUNT SET Balance = Balance - :Amount WHERE ID = :ID AND Balance >= :Amount;");
        m_prepare_query_balance = db->createStatement("SELECT Balance FROM ACCOUNT WHERE ID = :ID;");
    }

    DWORD AccountDbContext::Insert(Account *a) {
        Account *old = FindByFirstNameAndPIN(a->get_FirstName(), a->get_PIN());
        if (old != NULL) {
            return 1;
        }

        SQLite::Statement   *query = Get()->insertAccount;
        query->bind(":FirstName", a->get_FirstName()); // same as mquery->bind(1, aParamValue);
        query->bind(":LastName", a->get_LastName()); // same as mquery->bind(1, aParamValue);
        query->bind(":EmailAddress", a->get_EmailAddress()); // same as mquery->bind(1, aParamValue);
        query->bind(":PIN", a->get_PIN());
        query->bind(":SSNumber", (int)a->get_SSNumber());
        query->bind(":DLNumber", (int)a->get_DLNumber());

        // Loop to execute the query step by step, to get one a row of results at a time
        query->exec();
        // Reset the query to be able to use it again later
        query->reset();
        return 0;
    }

    Account *AccountDbContext::FindById(int ID) {
        SQLite::Statement   *query = Get()->findById;
        query->bind(":id", ID);
        Account *a = GetOne(query);
        query->reset();
        return a;

    }

    Account *AccountDbContext::FindByFirstNameAndPIN(const char *FirstName, int PIN) {
        SQLite::Statement  *query = Get()->findByFirstNameAndPin;
        query->bind(":FirstName", FirstName);
        query->bind(":PIN", PIN);
        Account *a = GetOne(query);
        query->reset();
        return a;
    }

    DWORD AccountDbContext::Deposit(DWORD AccountID, DWORD Amount) {
        SQLite::Transaction transaction(Db());
        DWORD affected = 0;

        // Step 2: Transaction, avoid database lock
        if (TransactionLogDbContext::Deposit(AccountID, Amount) == 0) {
            return 0;
        }

        // Step 1: Update Ammount
        SQLite::Statement *query = Get()->m_prepare_deposit;
        query->bind(":Amount", (const int)Amount);
        query->bind(":ID", (const int)AccountID);
        affected = query->exec();
        query->reset();
        if (affected == 0) {
            return 0;
        }



        // Commit transaction
        transaction.commit();
        return 1;
    }

    DWORD AccountDbContext::Withdrawal(DWORD AccountID, DWORD Amount) {
        SQLite::Transaction transaction(Db());
        int affected = 0;

        // Step 1: Update Ammount
        SQLite::Statement *query = Get()->m_prepare_widthdrawal;
        query->bind(":Amount", (const int)Amount);
        query->bind(":ID", (const int)AccountID);
        affected = query->exec();
        query->reset();
        if (affected == 0) {
            return 0;
        }

        // Step 2: Transaction, avoid database lock
        if (TransactionLogDbContext::Withdrawal(AccountID, Amount) == 0) {
            // rollback
            return 0;
        }


        // Commit transaction
        transaction.commit();
        return 1;
    }


    Account *AccountDbContext::GetOne(SQLite::Statement *query) {
        Account *a = new Account();
        if (query->executeStep()) {
            a->SetID(query->getColumn(0).getInt());
            a->SetFirstName(query->getColumn(1).getText());
            a->SetLastName(query->getColumn(2).getText());
            a->SetEmailAddress(query->getColumn(3).getText());
            a->SetPIN(query->getColumn(4).getInt());
            a->SetSSNumber(query->getColumn(5).getInt());
            a->SetDLNumber(query->getColumn(6).getInt());
            return a;
        }
        delete a;
        return NULL;
    }

    DWORD AccountDbContext::QueryBalance(DWORD AccountID, DWORD *Balance) {
        SQLite::Statement *query = Get()->m_prepare_query_balance;
        DWORD row = 0;
        query->bind(":ID", (const int)AccountID);
        if (query->executeStep()) {
            *Balance = query->getColumn(0).getInt();
            row = 1;
        }

        query->reset();
        return row;
    }

}
