#ifndef DBCONTEXT_H
#define DBCONTEXT_H

#include <string>
#include <cstring>
#include "sqlite3/Database.h"
#include "sqlite3/Transaction.h"

using namespace SQLite;

namespace model {

#define DEFAULT_DB_FILENAME "default.db3"

/// Object Oriented Basic example

class DbContext : public Database
{

public:


    DbContext() :  Database(DbContext::filename, SQLITE_OPEN_CREATE | SQLITE_OPEN_READWRITE) {

    }

    static void Init(const char* dbFilename = DEFAULT_DB_FILENAME);

    static DbContext *Get();
    static Database &Db();
    static Statement *createStatement(const char* sql);
    static const char* getFilename() {
        return DbContext::filename;
    }

public:

    static char* filename;

};

}


#endif // DBCONTEXT_H
