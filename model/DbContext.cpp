#include "DbContext.h"
#include <iostream>

using namespace SQLite;

namespace model {
    char* DbContext::filename = (char*)DEFAULT_DB_FILENAME;
    static DbContext* context = NULL;

    void DbContext::Init(const char* dbFilename) {
        DbContext::filename = strdup(dbFilename);
        context = NULL;
    }

    DbContext *DbContext::Get() {
        if (context == NULL) {
            context = new DbContext();
        }
        return context;
    }

     Database &DbContext::Db() {
        return *((Database*)Get());
     }

     Statement *DbContext::createStatement(const char *sql) {
         return new Statement(Db(), sql);
     }

}
