#ifndef ACCOUNT_H
#define ACCOUNT_H

#include "net/CNP_Protocol.h"
#include "model/DbContext.h"

using namespace std;
using namespace cnp;

namespace model {


// set structure alignment to 1 byte
#pragma pack(push, 1)

struct Account : CREATE_ACCOUNT_REQUEST {

    Account() : ID(0), Balance(0) {};
    Account(DWORD id) : ID(id), Balance(0) {};
    Account(const char *FirstName, WORD wPIN) : ID(0), Balance(0)
    {
        SetFirstName(FirstName);
        SetPIN(wPIN);
    };

    Account(CREATE_ACCOUNT_REQUEST *r) : CREATE_ACCOUNT_REQUEST(*r), ID(0), Balance(0) {};

    DWORD GetID() {
        return ID;
    }

    void SetID(int id) {
        ID = id;
    }

    void SetFirstName(const char* szSet)
    {
        m_Request.set_FirstName(szSet);
    };

    /**
        @param [in] szSet
    */
    void SetLastName(const char* szSet)
    {
        m_Request.set_LastName(szSet);
    };

    /**
        @param [in] szSet
    */
    void SetEmailAddress(const char* szSet)
    {
        m_Request.set_EmailAddress(szSet);
    };

    void SetPIN(WORD n) {
        m_Request.m_wPIN = n;
    }

    void SetSSNumber(DWORD n) {
        m_Request.m_dwSSNumber = n;
    }

    void SetDLNumber(DWORD num) {
       m_Request. m_dwDLNumber = num;
    }

    void print();
private:
    DWORD ID;
    DWORD Balance;
};

// restore the default structure alignment
#pragma pack(pop)


class AccountDbContext : public DbContext
{
public:
    static AccountDbContext *Get();


    static DWORD Insert(Account *a);

    static Account *FindById(int ID);

    static Account *FindByFirstNameAndPIN(const char *FirstName, int PIN) ;

    static Account *GetOne(SQLite::Statement *query);

    static DWORD Deposit(DWORD AccountID, DWORD Amount);
    static DWORD Withdrawal(DWORD AccountID, DWORD Amount);
    static DWORD QueryBalance(DWORD AccountID, DWORD *Balance);

private:

    SQLite::Statement *insertAccount;
    SQLite::Statement *findById;
    SQLite::Statement *findByFirstNameAndPin;
    SQLite::Statement *m_prepare_deposit;
    SQLite::Statement *m_prepare_widthdrawal;
    SQLite::Statement *m_prepare_query_balance;
    DbContext *db;

    AccountDbContext();

    static string CreateTable();

};

}
#endif // ACCOUNT_H
