#ifndef TRANSACTION_H
#define TRANSACTION_H


#include "net/CNP_Protocol.h"
#include "model/DbContext.h"

using namespace std;
using namespace cnp;

namespace model {

#pragma pack(push, 1)
struct TransactionLog : cnp::TRANSACTION
{

    TransactionLog() {};

    TransactionLog(DWORD AccountID, DWORD Amount, WORD Type) {
        m_dwID = AccountID;
        m_qwDateTime = time(NULL);
        m_dwAmount = Amount;
        m_wType = Type;
    };

    TransactionLog(DWORD AccountID, DWORD Amount, WORD Type, QWORD qwDateTime) {
        m_dwID = AccountID;
        m_qwDateTime = qwDateTime;
        m_dwAmount = Amount;
        m_wType = Type;
    };

    DWORD getAccountID() {
        return m_dwID;
    };

    QWORD getTimestamp() {
        return m_qwDateTime;
    }

    DWORD getAmount() {
        return m_dwAmount;
    }

    WORD getType() {
        return m_wType;
    }



    void print();
};

#pragma pack(pop)

class TransactionLogDbContext
{
public:
    static TransactionLogDbContext *Get();

    static DWORD Deposit(DWORD AccountID, DWORD Amount);
    static DWORD Withdrawal(DWORD AccountID, DWORD Amount);
    static TransactionLog *FindRange(DWORD AccountID, DWORD Offset, DWORD Limit, DWORD *Count);
    static TransactionLog *GetOne(SQLite::Statement *query);


private:

    SQLite::Statement *insertTransactionLog;;

    SQLite::Statement *findRange;
    DbContext *db;

    TransactionLogDbContext();

    static string CreateTable();
    static DWORD Insert(TransactionLog *t);
};

}

#endif // TRANSACTION_H
